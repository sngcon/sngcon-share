package sngcon_system

import (
	"sync"
)


const SYSTEM_STATUS_OK = 200
const SYSTEM_STATUS_OK_STR = "Ok"
const SYSTEM_STATUS_SERVER_PROBLEM = 500
const SYSTEM_STATUS_UNKNOWN_STR = "Unknown"

type SSystem struct {
	storage	  sync.Map
}

type SSystemStatus struct {
	Status    string
	StatusInt int
}


func (s SSystem) StatusInt() int {
	var result, ok = s.storage.Load("system:statusint")
	if ok {
		return result.(int)
	} else {
		return SYSTEM_STATUS_SERVER_PROBLEM	
	}
}

func (s SSystem) Status() string {
	var (
		statusint = s.StatusInt()
	)

	switch statusint {
		case SYSTEM_STATUS_OK:		
	    	return SYSTEM_STATUS_OK_STR
		default:
	    	return SYSTEM_STATUS_UNKNOWN_STR
	}
}

func (s *SSystem) SetStatusInt(statusint int ) {
	s.storage.Store("system:statusint",statusint)
}
